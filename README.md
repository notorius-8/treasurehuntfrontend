# TreasureHunt application
TreasureHunt application is an android multiplayer game  
were you can explore the world around you, find hidden riddles, answer them, collect points and win.  
You can either play alone or with your friends (recommended for extra fun).

## TreasureHunt Front-end
TreasureHunt front-end is the UI and Java code for the TreasureHunt application
## Technologies
* [Java](https://www.oracle.com/java/technologies/javase-downloads.html)
* [Retrofit](https://square.github.io/retrofit/) for an HTTP client.
* [Junit 4](https://junit.org/junit4/)/ [mockito](https://site.mockito.org/)/ [Espresso](https://developer.android.com/training/testing/espresso) for testing.

### Back-end
[Link to Back-end Repository](https://gitlab.com/notorius-8/treasurehuntbackend).
