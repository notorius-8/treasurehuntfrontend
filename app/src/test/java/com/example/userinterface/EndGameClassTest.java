package com.example.userinterface;

import com.example.userinterface.dto.RiddleDto;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EndGameClassTest {
public List<RiddleDto> riddleDtoList = new ArrayList<>();
RiddleDto riddleDto1 = new RiddleDto("hello","hello1","hello2","hello3","hello4",23.5543570414,41.0752080982,10);
RiddleDto riddleDto2 = new RiddleDto("hello","hello1","hello2","hello3","hello4",24.5543588675,41.075600115,10);

EndGameClass endGameClass = mock(EndGameClass.class);


    @Test
    public void checkIfEndGameMethodReturnTrue() {
        int pointsToWin=10 ;
        riddleDtoList.add(riddleDto1);
        riddleDtoList.add(riddleDto2);

        when(endGameClass.endGame(pointsToWin)).thenReturn(true);
        Boolean actualResult = endGameClass.endGame(pointsToWin);
        assertEquals(true,actualResult);
    }

    @Test
    public void checkIfGetPointsToWinReturnsPoints() {
        int pointsToWin=10 ;
        when(endGameClass.getPointsToWin()).thenReturn(pointsToWin);
        int actualResult = endGameClass.getPointsToWin();
        assertEquals(pointsToWin,actualResult);

    }
}