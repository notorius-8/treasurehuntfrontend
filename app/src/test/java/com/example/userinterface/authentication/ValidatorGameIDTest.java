package com.example.userinterface.authentication;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidatorGameIDTest {

    @Test
    public void checkIfGameIdIsNull(){
        String textGameId = "";
        ValidatorGameID service = new ValidatorGameID();

        assertTrue(service.checkIfGameIdIsNull(textGameId));

    }

    @Test
    public void checkIfGameIdIsFiveCharactersLong() {
        String textGameId = "AbcdF";
        ValidatorGameID service = new ValidatorGameID();
        assertFalse(service.checkIfGameIdIsFiveCharacters(textGameId));
    }

    @Test
    public void checkIfGameIdIsValid(){
        String textGameId = "AbcdF";
        ValidatorGameID service = new ValidatorGameID();

        assertFalse(service.checkIfGameIdIsValid(textGameId));
    }

}