package com.example.userinterface;

import com.example.userinterface.authentication.Validator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidatorTest {
    @Test
    public void checkIfPasswordsMatch() {
        String textPassword = "123456a", textConPassword = "123456a";
        Validator service = new Validator();

        assertTrue(service.checkIfPasswordsMatch(textPassword, textConPassword));
    }

    @Test
    public void checkIfPasswordIsStrong() {
        String textPassword = "1245a";
        Validator service = new Validator();

        assertFalse(service.checkIfPasswordIsStrong(textPassword));
    }

    @Test
    public void checkIfPasswordIsNull() {
        String textPassword = "";
        Validator service = new Validator();

        assertTrue(service.checkIfPasswordIsNull(textPassword));
    }

    @Test
    public void checkIfNameIsNull() {
        String textName = "John";
        Validator service = new Validator();

        assertFalse(service.checkIfNameIsNull(textName));
    }

    @Test
    public void checkIfSurnameIsNull() {
        String textSurname = "Doe";
        Validator service = new Validator();

        assertFalse(service.checkIfSurnameIsNull(textSurname));
    }

    @Test
    public void checkIfEmailIsValid() {
        String textEmail = "mns4@gmailcom";
        Validator service = new Validator();

        assertFalse(service.checkIfEmailIsValid(textEmail));
    }

}
