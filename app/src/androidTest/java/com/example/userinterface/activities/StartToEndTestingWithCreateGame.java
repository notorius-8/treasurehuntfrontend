package com.example.userinterface.activities;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import android.telephony.CarrierConfigManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;

import com.example.userinterface.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.content.Context.LOCATION_SERVICE;
import static androidx.core.content.ContextCompat.getSystemService;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.core.AllOf.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class StartToEndTestingWithCreateGame {

    @Rule
    public ActivityScenarioRule<MainActivity> mainActivityActivityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void endToEndCreateGame() throws UiObjectNotFoundException {
        SystemClock.sleep(2500);
        onView(withId(R.id.et_email)).perform(ViewActions.typeText("kanel@hotmail.com"));
        onView(isRoot()).perform(ViewActions.pressBack());
        onView(withId(R.id.et_password)).perform(ViewActions.typeText("123456"));
        onView(isRoot()).perform(ViewActions.pressBack());
        onView(withId(R.id.buttonLogin)).perform(ViewActions.click());
        SystemClock.sleep(2500);
        onView(withId(R.id.radioButton1)).perform(ViewActions.click());
        onView(withId(R.id.CreateGame)).perform(ViewActions.click());
        SystemClock.sleep(2500);
        UiDevice device = UiDevice.getInstance(getInstrumentation());
        UiObject marker = device.findObject(new UiSelector().descriptionContains("Riddle"));
        while(!onView(withId(R.id.myPoints)).equals("50")){
        if (marker.exists()) {
            marker.click();
            if (!marker.exists()){
            onView(withId(R.id.answer1)).perform(ViewActions.click());
            }
        }
        }
        onView(withId(R.id.seeResults)).perform(ViewActions.click());
    }

}

