package com.example.userinterface.activities;

import android.content.Intent;
import android.widget.Button;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.userinterface.R;
import com.example.userinterface.dto.RiddleDto;
import com.google.android.gms.maps.model.Marker;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class MapActivityTest {
//    List<RiddleDto> riddleDtoList;
//    RiddleDto riddleDto1 = new RiddleDto("riddle","correctAnswer","answer2","answer3","answer4",23.5543570414,41.0752080982,10);
//    RiddleDto riddleDto2 = new RiddleDto("riddle2","correctAnswer","answer2","answer3","answer4",23.5531081741,41.0748787012,10);
//    String gameId = "Xrlas";
//    String userName = "kanel";
//    UUID playerId;



    static Intent intent;
    static {
        String gameId = "Xrlas";
        String userName = "kanel";
        List<RiddleDto> riddleDtoList = new ArrayList<>();
        RiddleDto riddleDto1 = new RiddleDto("riddle","correctAnswer","answer2","answer3","answer4",23.5543570414,41.0752080982,10);
        RiddleDto riddleDto2 = new RiddleDto("riddle2","correctAnswer","answer2","answer3","answer4",23.5531081741,41.0748787012,10);
        riddleDtoList.add(riddleDto1);
        riddleDtoList.add(riddleDto2);
        intent = new Intent(ApplicationProvider.getApplicationContext(), MapActivity.class);
        intent.putExtra("list", (Serializable) riddleDtoList);
        intent.putExtra("gameId",gameId);
//        intent.putExtra("playerId", playerId);
        intent.putExtra("userName", userName);
    }

    static {
        String gameId = "Xrlas";
        String userName = "kanel";
        List<RiddleDto> riddleDtoList = new ArrayList<>();
        RiddleDto riddleDto1 = new RiddleDto("riddle","correctAnswer","answer2","answer3","answer4",23.5543570414,41.0762837335,10);
        RiddleDto riddleDto2 = new RiddleDto("riddle2","correctAnswer","answer2","answer3","answer4",23.5544775278,41.0748787012,10);
        riddleDtoList.add(riddleDto1);
        riddleDtoList.add(riddleDto2);
        intent = new Intent(ApplicationProvider.getApplicationContext(), MapActivity.class);
        intent.putExtra("list", (Serializable) riddleDtoList);
        intent.putExtra("gameId",gameId);
        intent.putExtra("userName", userName);
    }

    @Rule
    public ActivityScenarioRule<MapActivity> scenario = new ActivityScenarioRule<MapActivity>(intent);

    @Before
    public void setUp() {
//        riddleDtoList.add(riddleDto1);
//        riddleDtoList.add(riddleDto2);
//        Intent intent = new Intent();
//        intent.putExtra("list", (Serializable) riddleDtoList);
//        intent.putExtra("gameId", gameId);
////        intent.putExtra("playerId", playerId);
//        intent.putExtra("userName", userName);


//        Intent intent = new Intent();
//        riddleDtoList = (List<RiddleDto>)intent.getSerializableExtra("list");
//        gameId = (String) intent.getStringExtra("gameId");
//        playerId = (UUID) intent.getSerializableExtra("playerId");
//        userName = intent.getStringExtra("userName");
    }


    @Test
    public void checkIfMarkersWasPlaced() throws Exception {

        UiDevice device = UiDevice.getInstance(getInstrumentation());
        UiObject marker = device.findObject(new UiSelector().descriptionContains("Riddle"));
        marker.click();
        onView(withId(R.id.cancel_button)).perform(ViewActions.click());

    }

}