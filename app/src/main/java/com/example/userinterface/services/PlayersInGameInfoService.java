package com.example.userinterface.services;

import com.example.userinterface.clients.GameClient;
import com.example.userinterface.dto.PlayerProgressInfoDto;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PlayersInGameInfoService {
    private PlayersInGameInfoService.PlayersInGameInfoListener playersInGameInfoListener;

    public void getPlayersStats(String gameId) {
        Retrofit retrofit = RequestService.initializeRequest().build();
        GameClient client = retrofit.create(GameClient.class);
        Call<List<PlayerProgressInfoDto>> call = client.getPlayersInGameInfo(gameId);
        call.enqueue(new Callback<List<PlayerProgressInfoDto>>() {
            @Override
            public void onResponse(@NotNull Call<List<PlayerProgressInfoDto>> call, @NotNull Response<List<PlayerProgressInfoDto>> response) {
                if (playersInGameInfoListener != null)
                    playersInGameInfoListener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<PlayerProgressInfoDto>> call, Throwable t) {
                t.getCause();
                System.out.println(t);
                System.out.println("Error PlayerStats");
            }
        });
    }


    public interface PlayersInGameInfoListener {
        void onSuccess(List<PlayerProgressInfoDto> playerProgressInfoDtoList);

        void onFail(Throwable t);
    }

    public void setListener(PlayersInGameInfoService.PlayersInGameInfoListener playersInGameInfoListener) {
        this.playersInGameInfoListener = playersInGameInfoListener;
    }

}
