package com.example.userinterface.services;

import com.example.userinterface.clients.GameClient;
import com.example.userinterface.dto.GameStatusDto;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class GameStatusService {
    private GameStatusListener gameStatusListener;

    public void getGameStatus(String gameId) {
        Retrofit retrofit = RequestService.initializeRequest().build();
        GameClient client = retrofit.create(GameClient.class);
        Call<GameStatusDto> call = client.getGameStatus(gameId);
        call.enqueue(new Callback<GameStatusDto>() {
            @Override
            public void onResponse(@NotNull Call<GameStatusDto> call, @NotNull Response<GameStatusDto> response) {
                if (response.body() != null) {
                    if (gameStatusListener != null) {
                        gameStatusListener.onSuccess(response.body());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<GameStatusDto> call, Throwable t) {
                t.getCause();
                System.out.println(t);
            }
        });
    }

    public interface GameStatusListener {
        void onSuccess(GameStatusDto gameStatusDto);

        void onFail(Throwable t);
    }

    public void setListener(GameStatusListener gameStatusListener) {
        this.gameStatusListener = gameStatusListener;
    }


}
