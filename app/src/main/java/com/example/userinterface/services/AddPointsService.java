package com.example.userinterface.services;

import com.example.userinterface.clients.GameClient;
import com.example.userinterface.dto.RiddleDto;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddPointsService {
    private AddPointsService.AddPointsListener addPointsListener;

    public void addPoints(String gameId, UUID playerId, int points) {
        Retrofit retrofit = RequestService.initializeRequest().build();
        GameClient client = retrofit.create(GameClient.class);
        Call<Integer> call = client.addPointsToPlayer(gameId, playerId, points);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(@NotNull Call<Integer> call, @NotNull Response<Integer> response) {
                if (addPointsListener != null) {
                    addPointsListener.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(@NotNull Call<Integer> call, Throwable t) {
                t.getCause();
                System.out.println(t);
            }
        });
    }


    public interface AddPointsListener {
        void onSuccess(int points);

        void onFail(Throwable t);
    }

    public void setListener(AddPointsListener addPointsListener) {
        this.addPointsListener = addPointsListener;
    }

}
