package com.example.userinterface.services;

import com.example.userinterface.clients.GameClient;
import com.example.userinterface.dto.CreateGameDto;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CreateGameService {

    private CreateApiListener createGameListener;



    public void initializeCreateGame(UUID playerId) {
        Retrofit retrofit = RequestService.initializeRequest().build();
        GameClient client = retrofit.create(GameClient.class);
        Call<CreateGameDto> call = client.createGame(RiddleDtoService.getNumberOfRiddles(),playerId);
        call.enqueue(new Callback<CreateGameDto>() {
            @Override
            public void onResponse(@NotNull Call<CreateGameDto> call, @NotNull Response<CreateGameDto> response) {
                if (createGameListener != null)
                    createGameListener.onSuccess(response.body());
            }

            @Override
            public void onFailure(@NotNull Call<CreateGameDto> call, @NotNull Throwable t) {
               t.getCause();
            }
        });
    }


    public interface CreateApiListener {
        void onSuccess(CreateGameDto createGameDto);

        void onFail(Throwable t);
    }
    public void setListener(CreateApiListener createGameListener) {
        this.createGameListener = createGameListener;
    }

}

