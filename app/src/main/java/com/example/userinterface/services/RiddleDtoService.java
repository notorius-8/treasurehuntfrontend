package com.example.userinterface.services;

import com.example.userinterface.EndGameClass;
import com.example.userinterface.MarkerManager;
import com.example.userinterface.clients.RiddleDtoClient;
import com.example.userinterface.dto.RiddleDto;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RiddleDtoService {

    public ApiListenner mListener;

        private static int numberOfRiddles;

        public void initializeRiddleDtos (){
            Retrofit retrofit = RequestService.initializeRequest().build();
            RiddleDtoClient client = retrofit.create(RiddleDtoClient.class);
            Call<List<RiddleDto>> call = client.getRiddleDtos(getNumberOfRiddles());
            call.enqueue(new Callback<List<RiddleDto>>() {
                @Override
                public void onResponse(@NotNull Call<List<RiddleDto>> call, @NotNull Response<List<RiddleDto>> response) {
                    if (mListener != null)
                        mListener.onSuccess((List<RiddleDto>) response.body());
                    EndGameClass endGameClass = EndGameClass.getInstance();
                    endGameClass.setPointsToWin(response.body());
                }
                @Override
                public void onFailure(@NotNull Call<List<RiddleDto>> call, @NotNull Throwable t) {
                    System.out.println("Error");
                }
            });
        }


        public static int getNumberOfRiddles() {
            return numberOfRiddles;
        }

        public  void setNumberOfRiddles ( int numberOfRiddles){
           RiddleDtoService.numberOfRiddles = numberOfRiddles;
        }


        public interface ApiListenner {
            void onSuccess(List<RiddleDto> riddleDto);

            void onFail(Throwable t);
        }
    public void setListener(ApiListenner mListener) {
        this.mListener = mListener;
    }

    }
