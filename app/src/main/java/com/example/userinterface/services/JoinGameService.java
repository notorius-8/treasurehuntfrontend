package com.example.userinterface.services;

import com.example.userinterface.activities.LoginActivity;
import com.example.userinterface.activities.OptionsActivity;
import com.example.userinterface.clients.GameClient;
import com.example.userinterface.dto.JoinGameDTO;
import com.example.userinterface.dto.RiddleDto;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class JoinGameService {
    private JoinGameService.JoinApiListenner joinGameListener;

    public void initializeJoinGame(String gameId, UUID playerId) {
        Retrofit retrofit = RequestService.initializeRequest().build();
        GameClient client = retrofit.create(GameClient.class);
        Call<JoinGameDTO> call = client.joinGame(gameId, playerId);
        call.enqueue(new Callback<JoinGameDTO>() {
            @Override
            public void onResponse(@NotNull Call<JoinGameDTO> call,@NotNull Response<JoinGameDTO> response) {
                if (joinGameListener != null)
                    joinGameListener.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<JoinGameDTO> call, Throwable t) {
                t.getCause();
                System.out.println(t);
                System.out.println("Error join");
            }
        });
    }


    public interface JoinApiListenner {
        void onSuccess(JoinGameDTO joinGameDTO);

        void onFail(Throwable t);
    }

    public void setListener(JoinApiListenner joinGameListener) {
        this.joinGameListener = joinGameListener;
    }

}






