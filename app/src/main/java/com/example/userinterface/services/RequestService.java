package com.example.userinterface.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestService {


    private static String url = "https://notorious8-treasurehunt.herokuapp.com";

    public static Retrofit.Builder initializeRequest()
    {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson));

    }
}
