package com.example.userinterface.authentication;

public class Validator {

        private String message="";

        public boolean checkIfPasswordsMatch(String pass , String conPass){
            return pass.equals(conPass);
        }

        public boolean checkIfPasswordIsStrong(String pass){
            boolean strongPass = false;
            if(pass.length() >= 6) {
                strongPass = true;
            }
            return strongPass;
        }

        public boolean checkIfPasswordIsNull(String pass){
            boolean nullPass = false;
            if(pass.isEmpty()) {
                nullPass = true;
            }
            return  nullPass;
        }

        public boolean checkIfNameIsNull(String name){
            boolean nullName = false;
            if(name.isEmpty()) {
                nullName = true;
            }
            return  nullName;
        }

        public boolean checkIfSurnameIsNull(String surname){
            boolean nullName = false;
            if(surname.isEmpty()) {
                nullName = true;
            }
            return  nullName;
        }
    public boolean checkIfUsernameIsNull(String username){
        boolean nullName = false;
        if(username.isEmpty()) {
            nullName = true;
        }
        return  nullName;
    }

        public boolean checkIfEmailIsValid(String email){
            String expression = "^[a-zA-Z0-9._-]+@[a-z0-9.-]+\\.[a-z]{2,6}$";
            return email.matches(expression);
        }

        public boolean checkValidation(String textName, String textSurname,String textUsername, String textEmail, String textPassword,String textConPassword){
            boolean allClear = true;
            int passCode = 0;
            int matchCode = 0;

            Validator service = new Validator();

            if(service.checkIfNameIsNull(textName)){
                allClear = false;
                checkWarningStatus("Name field is empty");
            }

            if(service.checkIfSurnameIsNull(textSurname)){
                allClear = false;
                checkWarningStatus("Surname field is empty");
            }
            if(service.checkIfUsernameIsNull(textUsername)){
                allClear = false;
                checkWarningStatus("Username field is empty");
            }

            if(service.checkIfPasswordIsNull(textPassword)) {
                allClear = false;
                passCode = 1;
                checkWarningStatus("Password field is required");
            }

            if(!service.checkIfPasswordsMatch(textPassword,textConPassword)) {
                allClear = false;
                matchCode = 1;
                checkWarningStatus("Passwords don't match");
            }

            if(!service.checkIfPasswordIsStrong(textPassword)){
                allClear = false;
                if(passCode != 1 && matchCode !=1){
                    checkWarningStatus("Password must be at least 6 characters");}
            }

            if(!service.checkIfEmailIsValid(textEmail)){
                allClear = false;
                checkWarningStatus("Invalid e-mail");
            }

            return allClear;
        }


        public String checkWarningStatus(String alert){
            message+=alert+"\n";
            return  message;
        }

        public String clearMessage(){
            message="";
            return message;
        }

}

