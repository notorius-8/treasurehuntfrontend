package com.example.userinterface.authentication;

public class ValidatorGameID {
    private String message="";


    public boolean checkIfGameIdIsNull(String gameId){
        boolean nullGameId = false;
        if(gameId.isEmpty()){
            nullGameId = true;
        }
        return nullGameId;
    }

    public boolean checkIfGameIdIsFiveCharacters(String gameId){
        boolean fiveCharGameId = true;
        if(gameId.length() == 5){
            fiveCharGameId = false;
        }
        return fiveCharGameId;
    }

    public boolean checkIfGameIdIsValid(String gameId){
        String expression = "^[a-zA-z.-]+@[a-z.-]+\\.[a-z]{5}$";
        return gameId.matches(expression);

    }

    public boolean checkValidationId(String textGameId){
        boolean allClear = true;
        ValidatorGameID service = new ValidatorGameID();

        if(service.checkIfGameIdIsNull(textGameId)){
            allClear = false;
            checkWarning("Game ID is Null!!");
            return allClear;

        }

        if(service.checkIfGameIdIsFiveCharacters(textGameId)){
            allClear = false;
            checkWarning("Game ID isn't five characters long");
            return allClear;

        }
        if(service.checkIfGameIdIsValid(textGameId)){
            allClear = false;
            checkWarning("Game ID is valid");
            return allClear;
        }

        return allClear;
    }



    public String checkWarning(String alert){
        message+=alert+"\n";
        return message;
    }

    public String clearMessage(){
        message="";
        return message;
    }
}
