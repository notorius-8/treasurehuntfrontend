package com.example.userinterface;

import com.example.userinterface.dto.RiddleDto;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import static com.google.maps.android.SphericalUtil.computeDistanceBetween;

public class MarkerManager {
    private int DEFAULT_MARKER=100;
    private List<RiddleDto> riddleDtoList;
    private List<Marker> visibleMarkers = new ArrayList<>();
    private final List<Marker> markers = new ArrayList<Marker>();

    public List<Marker> placeMarkers(List<RiddleDto> list, GoogleMap mMap) {
        riddleDtoList = list;
        for (int i = 0; i < riddleDtoList.size(); i++) {
            Marker m = mMap.addMarker(new MarkerOptions().position(new LatLng(riddleDtoList.get(i).getLatitude(), riddleDtoList.get(i).getLongitude())).title("Riddle" + i));
            markers.add(m);
            m.setVisible(false);
        }
        return markers;
    }

    public List<Marker> turnVisibleNearbyMarkers(List<Marker> markers, LatLng playerPosition) {
        for (int i = 0; i < riddleDtoList.size(); i++) {
            if (!markers.get(i).getTitle().equals("Answered")) {
                double distance = computeDistanceBetween(playerPosition, markers.get(i).getPosition());
                markers.get(i).setVisible(distance <= 50);
            }
        }
        return visibleMarkers;
    }
    public int clickOnMarkerAction(Marker marker) {
        for (int i=0 ;i<riddleDtoList.size();i++){
            if (marker.getPosition().equals(new LatLng (riddleDtoList.get(i).getLatitude(),riddleDtoList.get(i).getLongitude())) && marker.getTitle().startsWith("Riddle")){
                return i;
            }
        }
        return DEFAULT_MARKER;
    }

}
