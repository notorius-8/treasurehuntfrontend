package com.example.userinterface.clients;

import com.example.userinterface.dto.CreateGameDto;
import com.example.userinterface.dto.GameInfoDto;
import com.example.userinterface.dto.GameStatusDto;
import com.example.userinterface.dto.JoinGameDTO;
import com.example.userinterface.dto.PlayerProgressInfoDto;
import com.example.userinterface.dto.RiddleDto;

import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface GameClient {

    @POST("/api/v1/joinGame/{gameId}/{playerId}")
    Call<JoinGameDTO> joinGame(@Path("gameId") String gameId, @Path("playerId") UUID playerId);

    @POST("/api/v1/createGame/{numberOfRiddles}/{playerId}")
    Call<CreateGameDto> createGame(@Path("numberOfRiddles") int numberOfRiddles, @Path("playerId") UUID playerId);

    @POST("/api/v1/addPointsToPlayer/{gameId}/{playerId}/{points}")
    Call<Integer> addPointsToPlayer(@Path("gameId") String gameId, @Path("playerId") UUID playerId, @Path("points") int points);

    @GET("/api/v1/getGameStatus/{id}")
    Call<GameStatusDto> getGameStatus(@Path("id") String gameId);

    @GET("/api/v1/getPlayersInGameInfo/{gameId}")
    Call <List<PlayerProgressInfoDto>> getPlayersInGameInfo(@Path("gameId") String gameId);

}
