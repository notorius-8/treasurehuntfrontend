package com.example.userinterface.clients;

import com.example.userinterface.dto.PlayerLoginDto;
import com.example.userinterface.models.Player;

import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PlayerClient {

    @GET("/api/v1/players")
    Call<List<Player>> getPlayers();

    @GET("/api/v1/players/{email}/{password}")
    Call<PlayerLoginDto> playerLogin(@Path("email") String email, @Path("password") String password);

    @POST("/api/v1/players")
    Call<Boolean> createPlayer(@Body Player player);

    @PUT("/api/v1/players/{id}")
    Call<Player> updatePlayer(@Path("id") UUID id, @Body Player updatePlayer);
}
