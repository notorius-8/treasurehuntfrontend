package com.example.userinterface.clients;

import com.example.userinterface.dto.RiddleDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RiddleDtoClient {

    @GET("/api/v1/riddleDto/random/{numberOfRiddleDtos}")
    Call<List<RiddleDto>> getRiddleDtos(@Path ("numberOfRiddleDtos") int numberOfDtos);

}
