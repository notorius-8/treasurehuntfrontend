package com.example.userinterface.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.userinterface.R;
import com.example.userinterface.dto.RiddleDto;
import com.example.userinterface.services.AddPointsService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class RiddleActivity extends AppCompatActivity {

    private int score;
    private int numberOfMarker;
    private Button answer1;
    private Button answer2;
    private Button answer3;
    private Button answer4;
    private TextView riddleQuestion;
    private String correctAnswer;
    private RiddleDto riddle;
    private String gameId;
    private UUID playerId;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riddle);
        if (getIntent().getExtras() != null) {
            numberOfMarker = (int) getIntent().getSerializableExtra("numberOfMarker");
            riddle = (RiddleDto) getIntent().getSerializableExtra("riddle");
            playerId = (UUID) getIntent().getSerializableExtra("playerId");
            gameId = getIntent().getStringExtra("gameId");
            score =(int) getIntent().getSerializableExtra("score");
        }

        riddleQuestion = findViewById(R.id.riddleQuestion);
        answer1 = findViewById(R.id.answer1);
        answer2 = findViewById(R.id.answer2);
        answer3 = findViewById(R.id.answer3);
        answer4 = findViewById(R.id.answer4);
        TextView pointsText = findViewById(R.id.pointsText);
        Button cancelButton = findViewById(R.id.cancel_button);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn = (Button) v;
                if (btn.getText().equals(correctAnswer)) {
                    AddPointsService addPointsService = new AddPointsService();
                    addPointsService.setListener(new AddPointsService.AddPointsListener() {
                        @Override
                        public void onSuccess(int points) {
                            Intent intent = new Intent();
                            intent.putExtra("score", (Serializable) points);
                            intent.putExtra("numberOfMarker", (Serializable) numberOfMarker);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                        @Override
                        public void onFail(Throwable t) {
                        }
                    });
                    addPointsService.addPoints(gameId, playerId, riddle.getPoints());


                    Toast.makeText(RiddleActivity.this, "Correct Answer! ", Toast.LENGTH_SHORT).show();
                    //ToDo tools so give to the player some benefits....
                } else {
                    Toast.makeText(RiddleActivity.this, "Wrong Answer!", Toast.LENGTH_SHORT).show();
                    finish();
                    //ToDo close riddle for some minutes...
                }

            }
        };

        answer1.setOnClickListener(onClickListener);
        answer2.setOnClickListener(onClickListener);
        answer3.setOnClickListener(onClickListener);
        answer4.setOnClickListener(onClickListener);
        pointsText.setText("Score: " + this.score);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setRiddle();
    }

    private void setRiddle() {
        correctAnswer = riddle.getCorrectAnswer();
        List<String> test = new ArrayList<>();
        test.add(riddle.getCorrectAnswer());
        test.add(riddle.getAnswer2());
        test.add(riddle.getAnswer3());
        test.add(riddle.getAnswer4());
        Collections.shuffle(test);

        riddleQuestion.setText(riddle.getRiddle());
        answer1.setText(test.get(0));
        answer2.setText(test.get(1));
        answer3.setText(test.get(2));
        answer4.setText(test.get(3));
    }
}

