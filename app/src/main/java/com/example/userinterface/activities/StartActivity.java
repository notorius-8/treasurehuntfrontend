package com.example.userinterface.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.userinterface.R;

public class StartActivity extends AppCompatActivity {

    Button btnLocal, btnOnline, btnHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        btnLocal = findViewById(R.id.btnLocal);
        btnOnline = findViewById(R.id.btnOnline);
        btnHelp = findViewById(R.id.btnHelp);

        btnLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), OptionsActivity.class));
                finish();
            }
        });


    }
}
