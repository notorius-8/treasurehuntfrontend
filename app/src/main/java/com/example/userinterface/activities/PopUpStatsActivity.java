package com.example.userinterface.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.userinterface.R;
import com.example.userinterface.dto.PlayerProgressInfoDto;
import com.example.userinterface.services.PlayersInGameInfoService;

import java.util.List;

public class PopUpStatsActivity extends Activity {
    private String gameId;
    private StringBuilder display;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_players_info);
        display = new StringBuilder();

        if (getIntent().getExtras() != null) {
            gameId = getIntent().getStringExtra("gameId");
        }
        TextView playerList = findViewById(R.id.playerList);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        getWindow().setLayout((int) (width * 0.6), (int) (height * 0.6));


        PlayersInGameInfoService playersInGameInfoService = new PlayersInGameInfoService();
        playersInGameInfoService.setListener(new PlayersInGameInfoService.PlayersInGameInfoListener() {
            @Override
            public void onSuccess(List<PlayerProgressInfoDto> playerProgressInfoDtoList) {
                for (PlayerProgressInfoDto p : playerProgressInfoDtoList) {
                    display.append(p.getUserName()).append(" ").append(p.getScore()).append("\n");
                }
                playerList.setText(display);
            }

            @Override
            public void onFail(Throwable t) {

            }
        });
        playersInGameInfoService.getPlayersStats(gameId);

    }
}