package com.example.userinterface.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.userinterface.R;

import java.util.UUID;

public class EndGameActivity extends AppCompatActivity {

    private Button seeResults;
    private Button btnPlayAgain;
    private int score;
    private String winner;
    private UUID playerId;
    private String userName;
    private int numOfRiddlesAnswered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent().getExtras() != null) {
            score = (int) getIntent().getSerializableExtra("score");
            winner = (String) getIntent().getStringExtra("winner");
            playerId = (UUID) getIntent().getSerializableExtra("playerId");
            userName = getIntent().getStringExtra("userName");
            numOfRiddlesAnswered = (int) getIntent().getSerializableExtra("numOfRiddlesAnswered");
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);

        seeResults = findViewById(R.id.seeResults);
        btnPlayAgain = findViewById(R.id.btnPlayagain);

        seeResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent(EndGameActivity.this, ResultActivity.class);
                resultIntent.putExtra("score",score);
                resultIntent.putExtra("winner",winner);
                resultIntent.putExtra("numOfRiddlesAnswered", numOfRiddlesAnswered);
                startActivity(resultIntent);
            }
        });

        btnPlayAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EndGameActivity.this , OptionsActivity.class);
                intent.putExtra("playerId", playerId);
                intent.putExtra("userName", userName);
                startActivity(intent);
                finish();
            }
        });


    }
}