package com.example.userinterface.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.userinterface.R;
import com.example.userinterface.authentication.ValidatorGameID;
import com.example.userinterface.dto.CreateGameDto;
import com.example.userinterface.dto.JoinGameDTO;
import com.example.userinterface.services.CreateGameService;
import com.example.userinterface.services.JoinGameService;
import com.example.userinterface.services.RiddleDtoService;

import java.io.Serializable;
import java.util.UUID;

public class OptionsActivity extends AppCompatActivity {

    private String gameIdIWantToJoin;
    private String userName;
    private String gameId;
    private EditText editTextGameId;
    private RadioGroup riddleQuantities;
    private Button joinGame;
    private Button createGame;
    private Button updatePlayer;
    private UUID playerId;
    private ValidatorGameID validatorGameID = new ValidatorGameID();
    private RiddleDtoService riddleDtoService = new RiddleDtoService();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        if (getIntent().getExtras() != null) {
            playerId = (UUID) getIntent().getSerializableExtra("playerId");
            userName = getIntent().getStringExtra("userName");
        }


        joinGame = findViewById(R.id.JoinGame);
        createGame = findViewById(R.id.CreateGame);
        updatePlayer = findViewById(R.id.UpdatePlayerData);
        riddleQuantities = findViewById(R.id.radioGroup);

        editTextGameId = findViewById(R.id.editTextView);


        createGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = riddleQuantities.getCheckedRadioButtonId();
                Button radioBtn = (RadioButton) findViewById(selectedId);

                if (radioBtn != null) {
                    int number = Integer.parseInt(radioBtn.getText().toString());
                    riddleDtoService.setNumberOfRiddles(number);

                    if (riddleDtoService.getNumberOfRiddles() == 0 && validatorGameID.checkValidationId(gameId)) {
                        Toast.makeText(getApplicationContext(), "Please choose an option to continue and put the gameID", Toast.LENGTH_SHORT).show();
                    } else {

                        CreateGameService createGameService = new CreateGameService();
                        createGameService.setListener(new CreateGameService.CreateApiListener() {

                            @Override
                            public void onSuccess(CreateGameDto createGameDto) {
                                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                                intent.putExtra("list", (Serializable) createGameDto.getRiddleDtoList());
                                intent.putExtra("gameId", createGameDto.getGameId());
                                intent.putExtra("scoreToWin", createGameDto.getScoreToWin());
                                intent.putExtra("playerId", playerId);
                                intent.putExtra("userName", userName);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onFail(Throwable t) {

                            }
                        });
                        createGameService.initializeCreateGame(playerId);
                    }
                } else {
                    Toast.makeText(OptionsActivity.this, "Please select number of riddles!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        joinGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextGameId == null && validatorGameID.checkValidationId(gameId)) {
                    Toast.makeText(OptionsActivity.this, "GameID is missing", Toast.LENGTH_SHORT).show();
                } else {
                    JoinGameService joinGameService = new JoinGameService();
                    joinGameService.setListener(new JoinGameService.JoinApiListenner() {
                        @Override
                        public void onSuccess(JoinGameDTO joinGameDTO) {
                            if (joinGameDTO == null) {
                                Toast.makeText(OptionsActivity.this, "Invalid gameId, please re-enter correct id.", Toast.LENGTH_SHORT).show();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                                intent.putExtra("list", (Serializable) joinGameDTO.getRiddleDtoList());
                                intent.putExtra("scoreToWin", joinGameDTO.getScoreToWin());
                                intent.putExtra("gameId", editTextGameId.getText().toString());
                                intent.putExtra("playerId", playerId);
                                intent.putExtra("userName", userName);
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFail(Throwable t) {

                        }
                    });
                    joinGameService.initializeJoinGame(editTextGameId.getText().toString(), playerId);
                }
            }
        });

        updatePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent updateIntent = new Intent(OptionsActivity.this, UpdateActivity.class);
                updateIntent.putExtra("playerId", playerId);
                startActivity(updateIntent);
                finish();
            }
        });

    }


    public String getGameIdIWantToJoin() {
        return gameIdIWantToJoin;
    }

    public void setGameIdIWantToJoin(String gameIdIWantToJoin) {
        this.gameIdIWantToJoin = gameIdIWantToJoin;
    }

    public void setUsername(String userName) {
        this.userName = userName;
    }

    public String getUsername() {
        return userName;
    }
}
