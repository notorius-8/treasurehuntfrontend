package com.example.userinterface.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.userinterface.R;
import com.example.userinterface.clients.PlayerClient;
import com.example.userinterface.dto.PlayerLoginDto;
import com.example.userinterface.models.Player;
import com.example.userinterface.services.RequestService;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {
    private EditText etEmail,etPassword;
    private UUID playerId;
    private String userName,email,password;
    private CheckBox saveLoginCheckBox;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;
    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        Button buttonLogin = findViewById(R.id.buttonLogin);
        TextView textViewRegisterHere = findViewById(R.id.textViewRegisterHere);
        saveLoginCheckBox= findViewById(R.id.saveLoginCheckBox);
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            etEmail.setText(loginPreferences.getString("email", ""));
            etPassword.setText(loginPreferences.getString("password", ""));
            saveLoginCheckBox.setChecked(true);
        }
        textViewRegisterHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == buttonLogin) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etEmail.getWindowToken(), 0);
                    email = etEmail.getText().toString();
                    password = etPassword.getText().toString();

                    if (saveLoginCheckBox.isChecked()) {
                        loginPrefsEditor.putBoolean("saveLogin", true);
                        loginPrefsEditor.putString("email", email);
                        loginPrefsEditor.putString("password", password);
                        loginPrefsEditor.commit();
                    } else {
                        loginPrefsEditor.clear();
                        loginPrefsEditor.commit();
                    }
                }
                    Retrofit retrofit = RequestService.initializeRequest().build();
                    PlayerClient client = retrofit.create(PlayerClient.class);
                    Call<PlayerLoginDto> call = client.playerLogin(etEmail.getText().toString(), etPassword.getText().toString());
                    call.enqueue(new Callback<PlayerLoginDto>() {
                        @Override
                        public void onResponse(@NotNull Call<PlayerLoginDto> call, @NotNull Response<PlayerLoginDto> response) {
                            if (!response.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, "The email or password is incorrect", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setIcon(R.drawable.ic_ckeck);
                            builder.setTitle("Login Successfully");
                            builder.setMessage("Welcome to Treasure Hunting");
                            playerId = response.body().getPlayerId();
                            userName = response.body().getUserName();
                            Intent intent = new Intent(LoginActivity.this, OptionsActivity.class);
                            intent.putExtra("playerId", playerId);
                            intent.putExtra("userName", userName);
                            LoginActivity.this.startActivity(intent);
                        }

                        @Override
                        public void onFailure(@NotNull Call<PlayerLoginDto> call, @NotNull Throwable t) {
                            Toast.makeText(LoginActivity.this, "No connection, please try again!", Toast.LENGTH_SHORT).show();
                            t.getCause();
                        }
                    });

            }
        });
    }

}
