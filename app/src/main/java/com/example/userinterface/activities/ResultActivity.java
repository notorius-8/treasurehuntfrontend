package com.example.userinterface.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.userinterface.R;
import com.example.userinterface.services.RiddleDtoService;

public class ResultActivity extends AppCompatActivity {
    private int score;
    private String winner;
    private int numOfRiddlesAnswered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent().getExtras() != null) {
            score = (int) getIntent().getSerializableExtra("score");
            winner = (String) getIntent().getStringExtra("winner");
            numOfRiddlesAnswered = (int) getIntent().getSerializableExtra("numOfRiddlesAnswered");
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        TextView winnerLabel = findViewById(R.id.winnerLabel);
        TextView resultLabel = findViewById(R.id.resultLabel);
        TextView totalScoreLabel = findViewById(R.id.totalScoreLabel);
        Button btnReturn = findViewById(R.id.btnReturn);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResultActivity.super.finish();
            }
        });

        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);

        resultLabel.setText("Riddles Answered : " + numOfRiddlesAnswered);
        totalScoreLabel.setText("Total Score : " + score);
        winnerLabel.setText("Winner : " + winner);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("totalScore", score);
        editor.commit();
    }
}