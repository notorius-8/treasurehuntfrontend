package com.example.userinterface.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.example.userinterface.MarkerManager;
import com.example.userinterface.R;
import com.example.userinterface.dto.GameStatusDto;
import com.example.userinterface.dto.RiddleDto;
import com.example.userinterface.services.GameStatusService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.UUID;


public class MapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private int indexOfMarker;
    private GoogleMap mMap;
    private TextView gameIdTv,myPoints;
    private Marker motionMarker = null;
    private LatLng playerPosition;
    private List<Marker> markers;
    private List<Marker> visibleMarkers;
    private List<RiddleDto> riddleDtoList;
    private LocationManager locationManager;
    private String gameId;
    private UUID playerId;
    private String userName;
    private int listenerHandler = 1;
    private int score;
    private TextView winScore = null;
    private int DEFAULT_MARKER = 100;
    private ProgressBar progressBar;
    private int scoreToWin;
    private MarkerManager markerManager;
    private int numOfRiddlesAnswered = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent().getExtras() != null) {
            riddleDtoList = (List<RiddleDto>) getIntent().getSerializableExtra("list");
            scoreToWin = getIntent().getIntExtra("scoreToWin", score);
            gameId = (String) getIntent().getStringExtra("gameId");
            playerId = (UUID) getIntent().getSerializableExtra("playerId");
            userName = getIntent().getStringExtra("userName");
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PackageManager.PERMISSION_GRANTED);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PackageManager.PERMISSION_GRANTED);

        markerManager = new MarkerManager();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        gameIdTv = findViewById(R.id.editTextGameID);
        gameIdTv.setText(gameId);
        gameIdTv.setTextSize(20F);
        mMap = googleMap;
        markers = markerManager.placeMarkers(riddleDtoList, mMap);
        mMap.setOnMarkerClickListener(this);
        winScore = findViewById(R.id.pointsText);
        winScore.setText("Score to win: " + scoreToWin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(scoreToWin);
        myPoints = (TextView) findViewById(R.id.myPoints);

        Button progressBtn = findViewById(R.id.Button);
        progressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapActivity.this, PopUpStatsActivity.class);
                intent.putExtra("gameId", gameId);
                startActivity(intent);

            }
        });

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {

                GameStatusService gameStatusService = new GameStatusService();
                gameStatusService.setListener(new GameStatusService.GameStatusListener() {
                    @Override
                    public void onSuccess(GameStatusDto gameStatusDto) {
                        if (gameStatusDto.isFinished()) {
                            if (listenerHandler < 10) {
                                Toast.makeText(MapActivity.this, gameStatusDto.getWinner() + " Won the game !!!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(MapActivity.this, EndGameActivity.class);
                                intent.putExtra("score", score);
                                intent.putExtra("winner", gameStatusDto.getWinner());
                                intent.putExtra("playerId", playerId);
                                intent.putExtra("userName", userName);
                                intent.putExtra("numOfRiddlesAnswered", numOfRiddlesAnswered);
                                startActivity(intent);
                                listenerHandler = 20;
                                MapActivity.super.finish();
                            }
                        }
                    }

                    @Override
                    public void onFail(Throwable t) {

                    }
                });
                gameStatusService.getGameStatus(gameId);

                playerPosition = new LatLng(location.getLatitude(), location.getLongitude());
                if (motionMarker == null) {
                    MarkerOptions options = new MarkerOptions().position(playerPosition).title(userName).icon(BitmapDescriptorFactory.fromResource(R.drawable.jones));
                    motionMarker = mMap.addMarker(options);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17.0f));
                } else {
                    motionMarker.setPosition(playerPosition);

                    visibleMarkers = markerManager.turnVisibleNearbyMarkers(markers, playerPosition);

                }
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        indexOfMarker = markerManager.clickOnMarkerAction(marker);
        if (indexOfMarker != DEFAULT_MARKER) {
            Intent intent = new Intent(MapActivity.this, RiddleActivity.class);
            intent.putExtra("riddle", (riddleDtoList.get(indexOfMarker)));
            intent.putExtra("gameId", gameId);
            intent.putExtra("numberOfMarker", indexOfMarker);
            intent.putExtra("playerId", playerId);
            intent.putExtra("score", score);
            startActivityForResult(intent, 1);
        }
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            score = (int) data.getSerializableExtra("score");
            numOfRiddlesAnswered++;
            indexOfMarker = (int) data.getSerializableExtra("numberOfMarker");
            markers.get(indexOfMarker).setVisible(false);
            markers.get(indexOfMarker).setTitle("Answered");
            progressBar.setProgress(score);
            myPoints.setText(String.valueOf("My Points: "+score));

        }
    }
}
