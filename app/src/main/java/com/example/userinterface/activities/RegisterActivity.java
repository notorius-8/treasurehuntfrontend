package com.example.userinterface.activities;


import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.userinterface.R;
import com.example.userinterface.authentication.Validator;
import com.example.userinterface.clients.PlayerClient;
import com.example.userinterface.models.Player;
import com.example.userinterface.services.RequestService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity {


    private EditText editTextName;
    private EditText editTextSurname;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private EditText editTextUsername;
    private Button buttonRegister;
    private Validator validator = new Validator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setTitle("Register");

        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextSurname = (EditText) findViewById(R.id.editTextSurname);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextConfirmPassword = (EditText) findViewById(R.id.editTextConfirmPassword);
        buttonRegister = (Button) findViewById(R.id.buttonRegister);

        buttonRegister.setOnClickListener(v -> {
            if (validator.checkValidation(
                    editTextName.getText().toString(),
                    editTextSurname.getText().toString(),
                    editTextUsername.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPassword.getText().toString(),
                    editTextConfirmPassword.getText().toString()
            )) {
                Player newPlayer = new Player(
                        editTextName.getText().toString(),
                        editTextSurname.getText().toString(),
                        editTextUsername.getText().toString(),
                        editTextEmail.getText().toString(),
                        editTextPassword.getText().toString()
                );
                sendNetworkRequest(newPlayer);

            } else {
                Toast.makeText(RegisterActivity.this, validator.checkWarningStatus(""), Toast.LENGTH_SHORT).show();
                validator.clearMessage();
            }
        });
    }

    private void sendNetworkRequest(Player player) {
        Retrofit retrofit = RequestService.initializeRequest().build();
        PlayerClient client = retrofit.create(PlayerClient.class);
        Call<Boolean> call = client.createPlayer(player);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.body().booleanValue()) {
                    RegisterActivity.super.finish();
                }else{
                    Toast.makeText(RegisterActivity.this, "Player not registered!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}


