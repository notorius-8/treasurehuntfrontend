package com.example.userinterface.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.userinterface.R;
import com.example.userinterface.authentication.Validator;
import com.example.userinterface.clients.PlayerClient;
import com.example.userinterface.models.Player;
import com.example.userinterface.services.RequestService;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UpdateActivity extends AppCompatActivity {

    private EditText editTextUpdateName;
    private EditText editTextUpdateSurname;
    private EditText editTextUpdateUsername;
    private EditText editTextUpdateEmail;
    private EditText editTextUpdatePassword;
    private EditText editTextUpdateConfirmPassword;
    private UUID playerId;
    private final Validator validator = new Validator();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_player);

        if (getIntent().getExtras() != null) {
            playerId = (UUID) getIntent().getSerializableExtra("playerId");
        }

        editTextUpdateName = findViewById(R.id.editTextUpdateName);
        editTextUpdateSurname = findViewById(R.id.editTextUpdateSurname);
        editTextUpdateUsername = findViewById(R.id.editTextUpdateUsername);
        editTextUpdateEmail = findViewById(R.id.editTextUpdateEmail);
        editTextUpdatePassword = findViewById(R.id.editTextUpdatePassword);
        editTextUpdateConfirmPassword = findViewById(R.id.editTextUpdateConfirmPassword);
        Button updateButton = findViewById(R.id.buttonUpdatePlayer);
        Button returnToLoginForm = findViewById(R.id.buttonReturnToLoginForm);

        updateButton.setOnClickListener(v -> {
            if(validator.checkValidation(
                    editTextUpdateName.getText().toString(),
                    editTextUpdateSurname.getText().toString(),
                    editTextUpdateUsername.getText().toString(),
                    editTextUpdateEmail.getText().toString(),
                    editTextUpdatePassword.getText().toString(),
                    editTextUpdateConfirmPassword.getText().toString()
            )) {
                Player updatePlayer = new Player(
                        editTextUpdateName.getText().toString(),
                        editTextUpdateSurname.getText().toString(),
                        editTextUpdateUsername.getText().toString(),
                        editTextUpdateEmail.getText().toString(),
                        editTextUpdatePassword.getText().toString()
                );
                updatePlayerNetworkRequest(playerId, updatePlayer);
            }
            else {
                Toast.makeText(UpdateActivity.this, validator.checkWarningStatus(""), Toast.LENGTH_SHORT).show();
                validator.clearMessage();
            }
        });

        returnToLoginForm.setOnClickListener(v -> {
            Intent intent = new Intent(UpdateActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        });

    }

    public void updatePlayerNetworkRequest(UUID playerId, Player updatePlayer)
    {
        Retrofit retrofit = RequestService.initializeRequest().build();
        PlayerClient client = retrofit.create(PlayerClient.class);
        Call<Player> call = client.updatePlayer(playerId,updatePlayer);

        call.enqueue(new Callback<Player>() {

            @Override
            public void onResponse(@NotNull Call<Player> call, @NotNull Response<Player> response) {
                if(response.isSuccessful()) {
                    Toast.makeText(UpdateActivity.this,"Player updated successfully!", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(UpdateActivity.this,"Player not updated!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Player> call, @NotNull Throwable t) {
                Toast.makeText(UpdateActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
