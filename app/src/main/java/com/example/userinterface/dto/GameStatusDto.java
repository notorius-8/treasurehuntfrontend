package com.example.userinterface.dto;

public class GameStatusDto {
    private boolean finished;
    private String winner;

    public GameStatusDto(boolean finished, String winner) {
        this.finished = finished;
        this.winner = winner;
    }

    public GameStatusDto() {
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
}
