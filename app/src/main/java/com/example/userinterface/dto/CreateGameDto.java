package com.example.userinterface.dto;

import java.util.List;

public class CreateGameDto {
    private String gameId;
    private List<RiddleDto> riddleDtoList;
    private int scoreToWin;

    public CreateGameDto(String gameId, List<RiddleDto> riddleDtoList, int scoreToWin) {
        this.gameId = gameId;
        this.riddleDtoList = riddleDtoList;
        this.scoreToWin = scoreToWin;
    }

    public CreateGameDto() {
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<RiddleDto> getRiddleDtoList() {
        return riddleDtoList;
    }

    public void setRiddleDtoList(List<RiddleDto> riddleDtoList) {
        this.riddleDtoList = riddleDtoList;
    }

    public int getScoreToWin() {
        return scoreToWin;
    }

    public void setScoreToWin(int scoreToWin) {
        this.scoreToWin = scoreToWin;
    }
}
