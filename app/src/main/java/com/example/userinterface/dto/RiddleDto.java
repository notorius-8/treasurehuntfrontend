package com.example.userinterface.dto;

import java.io.Serializable;

public class RiddleDto implements Serializable {
    private String riddle;
    private String correctAnswer;
    private String answer2;
    private String answer3;
    private String answer4;
    private double longitude;
    private double latitude;
    private int points;

    public RiddleDto(String riddle, String correctAnswer, String answer2, String answer3, String answer4, double longitude, double latitude, int points) {
        this.riddle = riddle;
        this.correctAnswer = correctAnswer;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.longitude = longitude;
        this.latitude = latitude;
        this.points = points;
    }

    public String getRiddle() {
        return riddle;
    }

    public void setRiddle(String riddle) {
        this.riddle = riddle;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setPoints(int points) { this.points = points; }

    public int getPoints() {
        return points;
    }
}
