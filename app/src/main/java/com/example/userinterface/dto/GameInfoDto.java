package com.example.userinterface.dto;

import java.io.Serializable;

public class GameInfoDto implements Serializable {
    private String gameId;
    private int numberOfPlayers;
    private int numberOfRiddles;


    public GameInfoDto(String gameId,int numberOfPlayers,int numberOfRiddles){
        this.gameId = gameId;
        this.numberOfPlayers = numberOfPlayers;
        this.numberOfRiddles = numberOfRiddles;
    }
    public String getGameId(){return gameId;}
    public void setGameId(String gameId) {this.gameId = gameId;}


    public int getNumberOfPlayers(int numberOfPlayers){return numberOfPlayers;}
    public void setNumberOfPlayers(int numberOfPlayers) { this.numberOfPlayers = numberOfPlayers; }

    public int getNumberOfRiddles() { return numberOfRiddles; }
    public void setNumberOfRiddles(int numberOfRiddles) { this.numberOfRiddles = numberOfRiddles; }
}
