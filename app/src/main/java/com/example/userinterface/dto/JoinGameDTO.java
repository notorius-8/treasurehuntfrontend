package com.example.userinterface.dto;

import java.util.List;

public class JoinGameDTO {

    private List<RiddleDto> riddleDtoList;
    private int scoreToWin;

    public JoinGameDTO() {
    }

    public JoinGameDTO(List<RiddleDto> riddleDtoList, int scoreToWin) {
        this.riddleDtoList = riddleDtoList;
        this.scoreToWin = scoreToWin;
    }

    public List<RiddleDto> getRiddleDtoList() {
        return riddleDtoList;
    }

    public void setRiddleDtoList(List<RiddleDto> riddleDtoList) {
        this.riddleDtoList = riddleDtoList;
    }

    public int getScoreToWin() {
        return scoreToWin;
    }

    public void setScoreToWin(int scoreToWin) {
        this.scoreToWin = scoreToWin;
    }
}
