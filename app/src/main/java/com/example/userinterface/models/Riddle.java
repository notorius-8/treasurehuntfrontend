package com.example.userinterface.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Riddle
{
    @SerializedName("riddle")
    @Expose
    private String riddle;

    @SerializedName("correctAnswer")
    @Expose
    private String correctAnswer;

    @SerializedName("answer2")
    @Expose
    private String answer2;

    @SerializedName("answer3")
    @Expose
    private String answer3;

    @SerializedName("answer4")
    @Expose
    private String answer4;

    public Riddle(String riddle, String correctAnswer, String answer2, String answer3, String answer4) {
        this.riddle = riddle;
        this.correctAnswer = correctAnswer;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
    }

    public String getRiddle() {
        return riddle;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public String getAnswer2() {
        return answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public String getAnswer4() {
        return answer4;
    }



}
