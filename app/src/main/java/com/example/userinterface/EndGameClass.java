package com.example.userinterface;

import com.example.userinterface.dto.RiddleDto;

import java.util.List;

public class EndGameClass {
    private static EndGameClass obj;

    private EndGameClass() {
    }

    public static EndGameClass getInstance() {
        if (obj == null)
            obj = new EndGameClass();
        return obj;
    }

    private int pointsToWin;

    public int getPointsToWin() {
        return pointsToWin;
    }

    public void setPointsToWin(List<RiddleDto> list) {
        for (RiddleDto r : list) {
            this.pointsToWin += r.getPoints();
        }
    }

    public boolean endGame(int points) {
        if (points == pointsToWin) {
            return true;
        }
        return false;
    }
}
