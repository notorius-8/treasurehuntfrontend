package com.example.userinterface;

import android.content.Context;

public class MyAppContext extends android.app.Application{
    private static MyAppContext instance;

    public static MyAppContext getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}

